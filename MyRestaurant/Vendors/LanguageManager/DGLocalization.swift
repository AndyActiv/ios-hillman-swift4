import UIKit

@objc protocol DGLocalizationDelegate {
    @objc optional func languageDidChanged(to:(String))
}

class DGLocalization:NSObject {
    
    weak var Delegate:DGLocalizationDelegate?
    
    //MARK:- Instance var
    var DEFAULTS_KEY_LANGUAGE_CODE = "DEFAULTS_KEY_LANGUAGE_CODE"
    var availableLocales = [Locale]() {
        didSet {
            self.setLanguage(withCode: availableLocales.first!)
        }
    }
    
    var currentLocale = Locale()
    
    //MARK:- Int
    override init() {
        let english = Locale().initWithLanguageCode(languageCode: "en", countryCode: "gb", name: "United Kingdom")
        self.availableLocales = [english as! Locale]
    }
    
    //MARK:- Singleton
    static let sharedInstance: DGLocalization = {DGLocalization()}()
    
    //MARK:- Methods
    func startLocalization(){
        
        let userDefaults = UserDefaults.standard
        let languageManager = DGLocalization.sharedInstance
        
        // Check if the language code has been already been set or not.
        let currentLanguage = userDefaults.string(forKey: DEFAULTS_KEY_LANGUAGE_CODE)
        
        if((userDefaults.string(forKey: DEFAULTS_KEY_LANGUAGE_CODE)) == nil){
            let currentLocale:NSLocale = NSLocale.current as NSLocale
            
            // GO through available localisations to find the matching one for the device locale.
            for locale in languageManager.availableLocales {
                if (locale.languageCode!) == (currentLocale.object(forKey: NSLocale.Key.languageCode) as! NSString) {
                    languageManager.setLanguage(withCode: locale)
                    break
                }
            }
            // If the device locale doesn't match any of the available ones, just pick the first one.
            if(((userDefaults.string(forKey: DEFAULTS_KEY_LANGUAGE_CODE))) == nil){
                languageManager.setLanguage(withCode:languageManager.availableLocales[0])
            }
        }
        else {
            languageManager.setLanguage(withCode: Locale().initWithLanguageCode(languageCode: currentLanguage! as NSString, countryCode: currentLanguage! as NSString, name: currentLanguage! as NSString) as! Locale)
        }
    }
    
    func addLanguage(newLang: Locale)  {
        self.availableLocales.append(newLang)
    }
    func getCurrentLanguage()->Locale {
        return currentLocale
    }
    
    func setLanguage(withCode langCode: AnyObject) {
        let langCode = langCode as! Locale
        UserDefaults.standard.set(langCode.languageCode, forKey:DEFAULTS_KEY_LANGUAGE_CODE)
        //delegate
        if let delegate = Delegate {
            delegate.languageDidChanged!(to: langCode.languageCode! as (String))
        }
        
        self.currentLocale = langCode
    }
    
    // DIP Return a translated string for the given string key.
    func getTranslationForKey(_ key: NSString)->NSString {
        
        // Get the language code.
        let languageCode =  UserDefaults.standard.string(forKey: DEFAULTS_KEY_LANGUAGE_CODE)
        
        // Get language bundle that is relevant.
        let bundlePath = Bundle.main.path(forResource: languageCode as String?, ofType: "lproj")
        let Languagebundle = Bundle(path: bundlePath!)
        
        // Get the translated string using the language bundle.
        let translatedString = Languagebundle?.localizedString(forKey: key as String, value:"", table: nil)
        return translatedString! as NSString;
    }
}


//MARK:- Locale
class Locale: NSObject {
    
    var name:NSString?
    var languageCode:NSString?
    var countryCode:NSString?
    
    func initWithLanguageCode(languageCode: NSString,countryCode:NSString,name: NSString)->AnyObject{
        self.name = name
        self.languageCode = languageCode
        self.countryCode = countryCode
        return self
    }
}


//MARK:- extension
//extension String {
//
//    var localize:String{
//        return DGLocalization.sharedInstance.getTranslationForKey(key: self as NSString) as String
//    }
//}

