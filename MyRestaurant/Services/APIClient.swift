//
//  APIService.swift
//  MyRestaurant
//
//  Created by Andy Nguyen on 6/2/18.
//  Copyright © 2018 Andy Nguyen. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

let qbBaseAPIProduction = "http://hillman.otsaw.com/"
let qbBaseAPIDevelopment = "http://hillmandev.otsaw.com/"
let qbBaseAPITesting = "http://hillman.otsaw.com/"

class APIClient : NSObject {
    
    static let shared = APIClient()
    var isInvalidToken = false
    
    func basePath() -> String {
        if isProduction == 1 {
            //PRODUCTION
            return qbBaseAPIProduction
        } else if isProduction == 2 {
            //DEVELOPMENT
            return qbBaseAPIDevelopment
        } else {
            //TESTING
            return qbBaseAPITesting
        }
    }
    
    var apiAccessToken:String! {
        get {
            return Defaults[.apiAccessToken] ?? ""
        }
        set {
            Defaults[.apiAccessToken] = newValue
        }
    }


    var apiRefreshToken:String! {
        get{
            return Defaults[.apiAccessToken] ?? ""
        }
        set {
            Defaults[.apiAccessToken] = newValue
        }
    }

    func updateAutheticationData(_ data:[String:AnyObject]) {
        if let refresh_token = data[apiKeyRefreshToken] as? String{
            self.apiRefreshToken = refresh_token
        }
        if let accessToken = data[apiKeyAccessToken] as? String {
            self.apiAccessToken = accessToken
        }
    }

    
    func doURLRequestWith(path:String,method: HTTPMethod = .get ,params : [String:Any]?,callerVC: BaseViewController?, urlEncoding: Alamofire.ParameterEncoding = URLEncoding.default, completion: @escaping ServiceHandler) {
        let URLString = (self.basePath()+path).addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)
        if !self.isInvalidToken {
            Alamofire.request(URLString!, method: method, parameters: params, encoding: urlEncoding, headers:["Authorization": "Bearer \(self.apiAccessToken!)"]).responseJSON(completionHandler: { (response:DataResponse<Any>) in
                guard let value = response.result.value else {
                    print("Error: did not receive data from \(path)")
                    completion(nil,response.result.error)
                    return
                }
                guard response.result.error == nil else {
                    print("error calling GET on \(path)")
                    print(response.result.error!.localizedDescription)
                    completion(nil,response.result.error)
                    return
                }
                let successData = JSON(value).dictionaryObject
                self.checkAndShowError(successData, viewController: callerVC)
                completion(successData,nil)
            })
           
        }
    }
    
    func checkAndShowError(_ result:[String:Any]?,viewController:BaseViewController?){
        if let result = result {
            if let data = result["error"] as? [String:AnyObject]
            {
                if let messages = data["validateError"] as? [String:[String]]
                {
                    viewController?.showErrorWithMessage(messages.first!.1.first!)
                } else {
                    if let message = data["message"] as? String {
                        viewController?.showErrorWithMessage(message)
                    }
                }
            }
        }
        
    }
    
}

extension DefaultsKeys {
    static let apiAccessToken = DefaultsKey<String?>(apiKeyAccessToken)
    static let apiRefreshToken = DefaultsKey<String?>(apiKeyRefreshToken)
}

extension APIClient {
    func CUS_postUsernameAndPasswordForLoginInfo(_ username:String, password:String, viewcontroller:BaseViewController?, completion: @escaping ServiceHandler) {
        let path  = "oauth/access_token"
        let params: [String:Any] = ["grant_type":"password","client_id":"f3d259ddd3ed8ff3843839b", "client_secret":"4c7f6f8fa93d59c45502c0ae8c4a95b", "username":username,"password" : password]
        doURLRequestWith(path: path, method: .post, params: params, callerVC: viewcontroller ,completion: completion)
    }
    
    func COR_getUserType(viewcontroller:BaseViewController?, completion: @escaping ServiceHandler) {
        let path = "api/v1/users"
        let params: [String:Any] = [:]
        doURLRequestWith(path: path, method: .get, params: params, callerVC: viewcontroller ,completion: completion)
    }
    
    func COR_getFloorList(viewcontroller:BaseViewController?, completion: @escaping ServiceHandler) {
        let path = "api/v1/floors"
        let params : [String:Any] = [:]
        doURLRequestWith(path: path, params: params, callerVC: viewcontroller, completion: completion)
    }
    
}
