//
//  DefineValue.swift
//  MyRestaurant
//
//  Created by Andy Nguyen on 12/2/18.
//  Copyright © 2018 Andy Nguyen. All rights reserved.
//

import Foundation
import UIKit

//Production    = 1,
//Development   = 2,
//Testing       = 3,
let isProduction = 2


//English   = 1
//Chinese   = 2
//Japanese  = 3
//var currentLanguage = 1

//English = en
//Chinese = zh-Hans
//Japanese = ja
//var languageCode = "zh-Hans"


//KEY AND VALUE

let apiKeyAccessToken = "access_token"
let apiKeyDeviceID = "device_id"
let apiKeyExpiresIn = "expires_in"
let apiKeyRefreshToken = "refresh_token"
let apiKeyTokenType = "token_type"
let apiKeyErrorMessage = "message"
let apiErrorKey = "error"


var kDefaultPasscode = "1234"
var kKitchenPrinterAtFloor = "kKitchenPrinterAtFloor"
var kDefaultRecieptPrinter = "kDefaultRecieptPrinter"

var kWaiterPass = "qaz"
///Default value

let kButtonCornerRadius : CGFloat = 7

///Define noti key
let kNotiAddedNewItem = "kNotiAddedNewItem"
let kNotiRetrieveItem = "kNotiRetrieveItem"
let kNotiUpdateOrderType = "kNotiUpdateOrderType"
let kNotiTableChangeStatus = "kNotiTableChangeStatus"
let kNotiUpdateFloor = "kNotiUpdateFloor"
let kNotiUpdateServiceRequest = "kNotiUpdateServiceRequest"
let kNotiUpdateQueueList = "kNotiUpdateQueueList"
let kLanguageChanged = "kLanguageChanged"
let kNotiDiscountGroupUpdate = "kNotiDiscountGroupUpdate"


let kDrinkCategoryID = [1,19]
let kGstTaxPercentage = "gstTaxPercentage"

//Notification

//Enum
var currentLoggedUser = userType.Waiter
var hideCategory = [2]
enum userType : Int {
    case Waiter = 1
    case Counter = 2
}

enum GuestQueueType : Int {
    case Waiting = 1
    case TakeAway = 2
    case All = 3
}

enum PaymentType: Int {
    case Normal = 1
    case Equal = 2
    case Custom = 3
    case FoodAndDrink = 4
}

enum PaymentMethod : Int {
    case Cash = 1
    case Card = 2
    case CashAndCard = 3
}

enum TransactionStatus : Int {
    case Ordering = 1
    case Serving = 2
    case Checkout = 3
    case Completed = 4
    func toString() -> String {
        switch self.rawValue {
        case 1:
            return "Ordering"
        case 2:
            return "Serving"
        case 3:
            return "Checkout"
        case 4:
            return "Completed"
        default:
            return "pls update orderstatus"
        }
    }
}


enum TableServiceRequest : Int{
    case Order = 1
    case Tea = 2
    case Checkout = 3
    case ChangePlate = 4
    case Others = 5
    func toString() -> String {
        switch self.rawValue {
        case 1:
            return "Order"
        case 2:
            return "TeaRefill"
        case 3:
            return "Checkout"
        case 4:
            return "ChangePlate"
        case 5:
            return "Others"
        default:
            return "Others"
        }
    }
}


let kColorForBackground = UIColor(rgb: 0x333333)
let kPrimaryColour = UIColor(rgb: 0xfa6c56)
let kPrimaryBackground = UIColor(rgb: 0xF2F2F2)
let kSideMenuBackground = UIColor(rgb: 0x290D4D)
let kTextboxBorderColour  = UIColor(rgb:0xCECED2)
let badgeBackgroundColour  = UIColor(rgb:0x08C4C4)
let kNavigationBarColour = UIColor.init(0xF0, green: 0xF0, blue: 0xF5)
