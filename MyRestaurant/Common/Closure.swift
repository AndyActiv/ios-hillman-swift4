//
//  Closure.swift
//  MyRestaurant
//
//  Created by Andy Nguyen on 12/2/18.
//  Copyright © 2018 Andy Nguyen. All rights reserved.
//

import Foundation

typealias ServiceHandler = (Any?, Error?) -> Void

