//
//  LoadingIndicator.swift
//  FIVMOONDRIVE
//
//  Created by OTSAW-DEV3 on 10/3/16.
//  Copyright © 2016 OTSAW-DEV3. All rights reserved.
//

import Foundation
import SVProgressHUD

class LoadingIndicator : NSObject {
    
    static let sharedInstance = LoadingIndicator()
    var minuteTimer = Timer()

    var processCount = 0
    var timeOut = 60
    
    func startNewProcess() {
        processCount += 1
        self.timeOut = 60
        if processCount == 1 {
            SVProgressHUD.setDefaultMaskType(.gradient)
            SVProgressHUD.show()
            self.addTimeout()
        }
    }
    
    func completeProcess() {
        if processCount > 0 {
            processCount -= 1
            if processCount == 0  {
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()

                }
                self.minuteTimer.invalidate()
            }
        }

    }
    
    @objc func countDown() {
        print(self.timeOut)
        if self.timeOut >= 1 {
            self.timeOut = self.timeOut - 1
        }
        if self.timeOut == 0 {
            completeProcess()
        }
    }
    
    func addTimeout() {
        self.minuteTimer.invalidate()
        self.minuteTimer =  Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(LoadingIndicator.countDown), userInfo: nil, repeats: true)
    }
}
