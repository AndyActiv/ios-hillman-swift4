//
//  String+Extension.swift
//  MyRestaurant
//
//  Created by Andy Nguyen on 13/2/18.
//  Copyright © 2018 Andy Nguyen. All rights reserved.
//

import Foundation

extension String {
    func localized(_ lang:String) -> String {
        let path = Bundle.main.path(forResource: lang, ofType: "lproj")
        let bundle = Bundle(path: path!)
        return NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: "")
    }
    
    func localize() -> String {
        return DGLocalization.sharedInstance.getTranslationForKey(self as NSString) as String
    }

}
