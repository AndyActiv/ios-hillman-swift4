//
//  UIView+Extension.swift
//  quabiipos
//
//  Created by Thaw Hein Thit on 8/8/16.
//  Copyright © 2016 OTSAW. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    
    func rounded() {
        self.layer.cornerRadius = self.frame.size.width / 2;
        self.clipsToBounds = true;
    }
    
//    func gradientWithColor(topColor:UIColor,bottomColor:UIColor) {
//        let gradientAddressView = UIView(frame: self.bounds)
//        let layer = CAGradientLayer()
//        layer.frame = self.layer.bounds
//        layer.colors = [topColor.CGColor, bottomColor.CGColor]
//        layer.cornerRadius = self.layer.cornerRadius
//        gradientAddressView.layer.addSublayer(layer)
//        self.addSubview(gradientAddressView)
//        self.sendSubviewToBack(gradientAddressView)
//        self.clipsToBounds = true
//    }
//    
//    func addFadeOnTopAndBottom(topColor:UIColor,bottomColor:UIColor) {
//        let gradientAddressView = UIView(frame: self.bounds)
//        gradientAddressView.alpha = 0.5
//        let layer = CAGradientLayer()
//        layer.frame = self.layer.bounds
//        layer.colors = [topColor.CGColor,UIColor.clearColor(),UIColor.clearColor(),UIColor.clearColor(), bottomColor.CGColor,bottomColor.CGColor]
//        layer.cornerRadius = self.layer.cornerRadius
//        gradientAddressView.layer.addSublayer(layer)
//        self.addSubview(gradientAddressView)
//        self.sendSubviewToBack(gradientAddressView)
//        self.clipsToBounds = true
//    }
    
}
