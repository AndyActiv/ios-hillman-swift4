//
//  UIButton+Extension.swift
//  quabiipos
//
//  Created by Andy Nguyen on 20/7/16.
//  Copyright © 2016 OTSAW. All rights reserved.
//

import Foundation
import UIKit

extension UIButton {
    func roundButton(){
        self.layer.cornerRadius = self.frame.size.width / 2;
        self.clipsToBounds = true;
    }

    func roundCornerWithPrimaryColor() {
        self.layer.borderColor = kPrimaryColour.cgColor
        self.layer.borderWidth = 1
        self.layer.cornerRadius = kButtonCornerRadius
    }
    
    func disableState() {
        self.isUserInteractionEnabled = false
        self.alpha = 0.7
    }
    
    func enableState() {
        self.isUserInteractionEnabled = true
        self.alpha = 1
    }
}
