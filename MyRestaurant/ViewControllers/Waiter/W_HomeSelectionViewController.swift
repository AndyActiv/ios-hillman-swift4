//
//  W_HomeSelectionViewController.swift
//  MyRestaurant
//
//  Created by Andy Nguyen on 20/2/18.
//  Copyright © 2018 Andy Nguyen. All rights reserved.
//

import UIKit

class W_HomeSelectionViewController: BaseViewController {

    lazy var waiter_menuStoryBoard : UIStoryboard = {
        return UIStoryboard(name: "Waiter_Menu", bundle: Bundle.main)
    }()
    
    lazy var takeOutViewController : W_TakeOutSelectionViewController = {
        let takeOutVC = self.waiterStoryBoard!.instantiateViewController(withIdentifier: "W_TakeOutSelectionViewController") as! W_TakeOutSelectionViewController
        return takeOutVC
    }()
    
    var selectTableViewController : W_SelectTableHomeViewController?

    lazy var waitingSelectionHomeViewController : W_WaitingSelectionHomeViewController = {
        let takeOutVC = self.waiterStoryBoard!.instantiateViewController(withIdentifier: "W_WaitingSelectionHomeViewController") as! W_WaitingSelectionHomeViewController
        return takeOutVC
    }()
    
    weak var waiterStoryBoard : UIStoryboard?
    
    @IBOutlet weak var dinningTypeSegment: UISegmentedControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBOutlet weak var container: UIView!
    
    @IBAction func segmentDidChange(_ sender: Any) {
        switch  self.dinningTypeSegment.selectedSegmentIndex {
        case 0:
            self.bringViewControllerToContainer(myViewController: self.selectTableViewController!)
        case 1:
            self.bringViewControllerToContainer(myViewController: self.takeOutViewController)
        case 2:
            self.bringViewControllerToContainer(myViewController: self.waitingSelectionHomeViewController)
        default:
            print(0)
        }
    }
    
    func bringViewControllerToContainer(myViewController:UIViewController) {
        self.addChildViewController(myViewController)
        myViewController.view.frame = CGRect(x: 0, y: 0, width: self.container.frame.size.width, height: self.container.frame.size.height)
//            CGRect(0, 0, self.container.frame.size.width, self.container.frame.size.height);
        self.container.addSubview(myViewController.view)
        myViewController.didMove(toParentViewController: self)
    }
    
    func hideViewControllerFromContainer(myViewController:BaseViewController) {
        myViewController.willMove(toParentViewController: nil)
        myViewController.view.removeFromSuperview()
        myViewController.removeFromParentViewController()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func customView() {
        super.customView()
        self.dinningTypeSegment.tintColor = kPrimaryColour
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "selectTableSegue"
        {
            if let vc = segue.destination as? W_SelectTableHomeViewController {
                self.selectTableViewController = vc
                self.selectTableViewController!.delegate = self
//                if self.categoryArray.count > 0 {
//                    self.showListDishesOfCategory(0)
//                }
//                vc.mainView = self
//                vc.orderDelegate = self
            }
            
        }
    }


}

extension W_HomeSelectionViewController : W_SelectTableHomeViewControllerDelegate {
    func didTapOnTableID(_ tableData: [String : AnyObject]) {
        let menuVC = self.waiter_menuStoryBoard.instantiateViewController(withIdentifier: "W_MenuViewController") as! W_MenuViewController
        self.navigationController?.pushViewController(menuVC, animated: true)
    }
}
