//
//  COR_TableItemCollectionViewCell.swift
//  quabiipos
//
//  Created by Andy Nguyen on 20/7/16.
//  Copyright © 2016 OTSAW. All rights reserved.
//

import UIKit

protocol COR_TableItemCollectionViewCellDelegate : class {
    func didTapOnTableID(_ tableData :[String:AnyObject])
}

class COR_TableItemCollectionViewCell: UICollectionViewCell {
    weak var delegate :COR_TableItemCollectionViewCellDelegate?
    var tableData :[String:AnyObject] = [:] {
        didSet {
            if let table_no = tableData["table_no"] as? String {
                self.tableBtn.setTitle(table_no, for: [.normal])
            }
            if let table_status = tableData["status"] as? Int {
                self.status = table_status
            }
        }
    }
    
    
    @IBOutlet weak var tableBtn: UIButton!
    
    var status:Int = 1 {
        didSet {
            if status == 1 {
                self.isAvailable(true)
            } else if status == 2 {
                self.isAvailable(false)
            } else if status == 3 {
                self.isAvailable(false)
                self.tableBtn.backgroundColor = UIColor.red
            } 
        }
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        self.tableBtn.layer.borderColor = backgroundColor?.cgColor
        self.tableBtn.layer.borderWidth = 1
        self.tableBtn.roundButton()
//        self.statusBadge.rounded()
//        self.statusBadge.backgroundColor = badgeBackgroundColour
    }
    
    func isAvailable(_ selected:Bool){
        if selected == true {
            self.tableBtn.backgroundColor = UIColor.white
            self.tableBtn.setTitleColor(kPrimaryColour, for: [.normal])
        } else if selected == false{
            self.tableBtn.backgroundColor = kPrimaryColour
            self.tableBtn.setTitleColor(UIColor.white, for: [.normal])
        }
    }
    
    @IBAction func tableTapped(sender: AnyObject) {
        self.delegate?.didTapOnTableID(self.tableData)
    }
    
}
