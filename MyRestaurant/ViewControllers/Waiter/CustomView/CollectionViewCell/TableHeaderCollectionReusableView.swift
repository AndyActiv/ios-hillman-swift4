//
//  TableHeaderCollectionReusableView.swift
//  MyRestaurant
//
//  Created by Andy Nguyen on 21/2/18.
//  Copyright © 2018 Andy Nguyen. All rights reserved.
//

import UIKit

class TableHeaderCollectionReusableView: UICollectionReusableView {
    @IBOutlet weak var headerLabel: UILabel!
    
}
