//
//  SelectTableHomeViewController.swift
//  MyRestaurant
//
//  Created by Andy Nguyen on 20/2/18.
//  Copyright © 2018 Andy Nguyen. All rights reserved.
//

import UIKit

protocol W_SelectTableHomeViewControllerDelegate : class {
    func didTapOnTableID(_ tableData :[String:AnyObject])
}

class W_SelectTableHomeViewController: BaseViewController {

    weak var delegate : W_SelectTableHomeViewControllerDelegate?
    @IBOutlet weak var collectionView: UICollectionView!
    var floorData : [[String:AnyObject]] = [] {
        didSet {
            for item in floorData {
                if let floorName = item["descriptions"] as? String {
                    floorSegment.append(floorName)
                }
            }
        }
    }
    
    var floorSegment : [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func customView() {
        LoadingIndicator.sharedInstance.startNewProcess()
        APIClient.shared.COR_getFloorList(viewcontroller: self) { (result, error) in
            if let result = result as? [String:AnyObject] {
                if let data = result["data"] as? [[String:AnyObject]] {
                    self.floorData = data
                }
            }
            LoadingIndicator.sharedInstance.completeProcess()
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        self.collectionView.reloadData()
    }

}

extension W_SelectTableHomeViewController : UICollectionViewDelegate, UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return floorData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        var totalTable = 0
        if self.floorData.count > 0 {
            print(floorData)
            let currentFloor = self.floorData[section]
            if let tableData = currentFloor["tables"] as? [[String:AnyObject]] {
                totalTable = tableData.count > 0 ? tableData.count : 0
            }
        }
        return totalTable
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let resuseIdentifier = "COR_TableItemCollectionViewCell"
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: resuseIdentifier, for: indexPath) as! COR_TableItemCollectionViewCell
        if let tableArr = self.floorData[indexPath.section]["tables"] as? [[String:AnyObject]] {
            cell.tableData = tableArr[indexPath.row]
        }
        cell.delegate = self
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if let sectionHeader = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "TableHeaderCollectionReusableView", for: indexPath) as? TableHeaderCollectionReusableView{
            if let title = self.floorData[indexPath.section]["floor_number"] as? String {
                sectionHeader.headerLabel.text = title
            }
            return sectionHeader
        }
        return UICollectionReusableView()
    }
    
   
}

extension W_SelectTableHomeViewController : COR_TableItemCollectionViewCellDelegate {
    func didTapOnTableID(_ tableData :[String:AnyObject]) {
        self.delegate?.didTapOnTableID(tableData)
//        self.dismiss(animated: true, completion: {
//        })
    }
}

