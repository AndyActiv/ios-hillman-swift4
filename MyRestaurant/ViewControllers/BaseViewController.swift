//
//  BaseViewController.swift
//  MyRestaurant
//
//  Created by Andy Nguyen on 12/2/18.
//  Copyright © 2018 Andy Nguyen. All rights reserved.
//

import UIKit
import ZAlertView

class BaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.customView()
    }
    
    func customView() {
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    var shouldHideNavigationBar = false {
        didSet {
            if shouldHideNavigationBar == true {
                self.navigationController?.setNavigationBarHidden(true, animated: false)
            } else {
                self.navigationController?.setNavigationBarHidden(false, animated: false)
                
            }
        }
    }

    func translate(){
        
    }
    
    func showErrorWithMessage(_ messageStr: String) {
        self.showPopupWithTitle("Error", messageStr: messageStr)
    }
    
    func showPopupWithTitle(_ title:String,messageStr: String) {
        let dialog = ZAlertView(title: title, message: messageStr, alertType: ZAlertView.AlertType.multipleChoice)
        dialog.addButton("OK", touchHandler: { alertView in
            alertView.dismiss(animated: true, completion: nil)
        })
        dialog.width = 250
        dialog.show()
    }
    
    func showPopupWithTitle(_ title: String, message: String, confirmTitle: String, confirmAction: @escaping (ZAlertView) -> ()) {
        let dialog = ZAlertView(title: title, message: message, alertType: ZAlertView.AlertType.multipleChoice)
        dialog.addButton(confirmTitle, touchHandler:confirmAction)
        dialog.width = 250
        dialog.show()
    }
    
    func showConfirmPopupWithTitle(_ title: String, message: String, confirmTitle: String, confirmAction: @escaping (ZAlertView) -> ()) {
        let dialog = ZAlertView(title: title, message: message, alertType: ZAlertView.AlertType.multipleChoice)
        dialog.addButton(confirmTitle, touchHandler:confirmAction)
        dialog.addButton("Cancel".localize()) { (alertView) in
            alertView.dismiss(animated:  true)
        }
        dialog.width = 400
        dialog.height = 300
        dialog.show()
    }
    
    func dismissSelf() {
        self.dismiss(animated:true, completion: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
