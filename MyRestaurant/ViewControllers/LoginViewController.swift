//
//  ViewController.swift
//  MyRestaurant
//
//  Created by Andy Nguyen on 6/2/18.
//  Copyright © 2018 Andy Nguyen. All rights reserved.
//

import UIKit
import ZAlertView

class LoginViewController: BaseViewController {
    @IBOutlet weak var usernameTextfield: UITextField!
    @IBOutlet weak var passwordTextfield: UITextField!
    lazy var waiterStoryBoard : UIStoryboard = {
        return UIStoryboard(name: "Waiter", bundle: Bundle.main)
    }()
    
    
    @IBOutlet weak var loginBtn: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.shouldHideNavigationBar = true
        if isProduction == 2 {
            self.autoInputMockACC()
        }
    }
    
    @IBAction func loginBtnTapped(sender: AnyObject) {
        if !self.usernameTextfield.text!.isEmpty {
            if !self.passwordTextfield.text!.isEmpty {
                self.loginBtn.isUserInteractionEnabled = false
                self.loginWithUsername(self.usernameTextfield.text, and: self.passwordTextfield.text)
            } else {
                self.showErrorWithMessage("Please input your password")
            }
        } else {
            self.showErrorWithMessage("Please input your username")
        }
    }
    
    func autoInputMockACC()
    {
        if UIScreen.main.nativeBounds.size.height == 2732 {
            self.usernameTextfield.text = "c@hm.com"
            self.passwordTextfield.text = "123456"
        } else {
            self.usernameTextfield.text = "w@hm.com"
            self.passwordTextfield.text = "123456"
            kWaiterPass = "123456"
        }
    }
    
    func loginWithUsername(_ username: String!, and password:String!) {
        LoadingIndicator.sharedInstance.startNewProcess()
        APIClient.shared.CUS_postUsernameAndPasswordForLoginInfo(self.usernameTextfield.text!, password: self.passwordTextfield.text!, viewcontroller: self) { (result,error) in
            if let result = result as? [String:AnyObject] {
                if let _ = result["refresh_token"] as? String {
                    APIClient.shared.updateAutheticationData(result)
                    APIClient.shared.COR_getUserType(viewcontroller: self, completion: { (result, error) in
                        if let result = result as? [String:AnyObject] {
                            if let data = result["data"] as? [String:AnyObject] {
                                if let roleName = data["role_name"] as? String {
                                    if roleName == "Waiter" {
                                        currentLoggedUser = userType.Waiter
                                        self.showWaiterScreen()
                                    } else if roleName == "Counter" {
                                        currentLoggedUser = userType.Counter
//                                        self.showCounterScreen()
                                    }
                                    kDefaultPasscode = password
//                                    TransactionManager.sharedInstance.clearCurrentTransaction()
                                }
                            }
                            
                        }
                    })
                }
            }
            self.loginBtn.isUserInteractionEnabled = true
            LoadingIndicator.sharedInstance.completeProcess()
        }
    }

    func showWaiterScreen() {
        let vc = self.waiterStoryBoard.instantiateViewController(withIdentifier: "W_HomeSelectionViewController") as! W_HomeSelectionViewController
        vc.waiterStoryBoard = self.waiterStoryBoard
        self.navigationController?.pushViewController(vc, animated: true)
    }

}

